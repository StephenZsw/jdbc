在这里记录一下 此项目：
    主要就是基于实体类上的注解动态生成sql语句，达到不用写sql的功能，同时也是开放了手动写sql的方法，满足复杂sql的需求。

主要的几个类：

        连接池数据结构实现ConnectionHolder 用ArrayDeque来实现的。

        主要方法就是初始化读取配置文件来初始化连接池，一个连接对象包括了获取时间和connection对象，过期自动从栈中删除。

        连接池数据ConnectionPool 主要是拿来获取连接的，每次从ConnectionHolder 中获取一个连接，
        并利用动态代理代理这个connection，这样做的好处就是每次在close connection的时候可以进入增强逻辑，就是放回connection连接而不是真的直接close。

        事务管理器：TransactionManager,
        基于aspectj来实现，在需要进行事务控制的方法上注解上对应的注解即可。

        同时为了可以达到事务控制 一个方法里所有的数据库操作都需要是同一个connection，
        保证是同一个连接同一个事务。这个时候可以在第一次获取connection的时候把connection存入threadlocal，保证当前线程同一事务。
