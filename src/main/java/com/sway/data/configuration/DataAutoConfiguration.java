package com.sway.data.configuration;


import com.sway.data.core.DataTemplate;
import com.sway.data.core.SwayDataSource;
import com.sway.data.pool.ConnectionHolder;
import com.sway.data.pool.ConnectionPool;
import com.sway.data.transation.TransactionManager;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass({com.mysql.cj.jdbc.Driver.class,com.mysql.jdbc.Driver.class})
@EnableConfigurationProperties({com.sway.data.pool.ConnectionHolder.class,com.sway.data.core.SwayDataSource.class})
public class DataAutoConfiguration {



    @Bean
    public ConnectionPool connectionHolder(SwayDataSource dataSource,ConnectionHolder connectionHolder){
        return new ConnectionPool(dataSource,connectionHolder);
    }

    @Bean
    public DataTemplate dataTemplate(ConnectionPool connectionPool){
        return new DataTemplate(connectionPool);
    }

    @Bean
    public TransactionManager transactionManage(ConnectionPool connectionPool){
        return new TransactionManager(connectionPool);
    }

}
