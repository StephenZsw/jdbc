package com.sway.data.pool;

import java.sql.Connection;

public interface IPool {

    Connection getConnection();

    void close(Connection connection);

}
