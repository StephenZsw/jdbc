package com.sway.data.exception;

public class ResultMapperException extends RuntimeException{

    public ResultMapperException(String msg){
        super(msg);
    }
}
