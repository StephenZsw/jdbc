package com.sway.data.exception;

public class BuildingSqlException extends RuntimeException{

    public BuildingSqlException(String msg){
        super(msg);
    }
}
