package com.sway.data;

import com.sway.data.annotation.Column;
import com.sway.data.annotation.Operator;
import com.sway.data.annotation.Table;

@Table("admin")
public class Admin {

    @Operator(name="=",column ="adminId" )
    @Column("adminId")
    public Integer id;
    @Column("adminName")
    public String adminName;
    @Column("sex")
    public String sex;
    @Column("age")
    public Integer age;

    @Override
    public String toString() {
        return "Admin{" +
                "id=" + id +
                ", adminName='" + adminName + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                '}';
    }
}
