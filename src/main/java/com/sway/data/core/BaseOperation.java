package com.sway.data.core;

import java.util.List;

public interface BaseOperation {
    <T> List<T> select(T search);

    <T> List<T> select(String sql, Class<T> view, Object... args);
    <T> Integer update(String sql, Class<T> view, Object... args);
    <T> Integer insert(String sql, Class<T> view, Object... args);

    <T> void update(T update);
    <T> void delete(T delete);
    <T> void insert(T entity);

    <T> void batchInsert(List<T> entities);
}
