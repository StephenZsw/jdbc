package com.sway.data.core;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public interface BaseMapper<T> {
//    Map<String, Field> map();

    Field get(String key);

    List<T> result(ResultSet resultSet);

    void mapper(T target,Field field,ResultSet resultSet,int index);
}
