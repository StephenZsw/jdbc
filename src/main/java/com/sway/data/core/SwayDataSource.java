package com.sway.data.core;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
@ConfigurationProperties(prefix = "spring.sway.data")
public class SwayDataSource {

    private String driver;
    private String username;
    private String password;
    private String url;

    public Connection build(){
        try {
            Class.forName(driver);
            return DriverManager.getConnection(this.url,this.username,this.password);
        } catch (Exception e) {

            throw new RuntimeException("please check your properties, make sure there are not missing spring.sway.data. or"+e.getMessage());
        }
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
