package com.sway.data.core;

public enum SqlType {

    SELECT,
    UPDATE,
    INSERT,
    DELETE,
    BATCH_INSERT
}
